var email = context.getVariable("request.queryparam.email");
print(email);
var cdgId = context.getVariable("request.queryparam.cdgId");
print(cdgId);
var path = "/customers/"+email+"/newExperience/"+cdgId;
print(path);
context.setVariable("apiPath",path);
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);

// validations on cdgId and email query params
context.setVariable("isValidRequest","true");

if(cdgId === null || cdgId === "") {
    context.setVariable("isValidRequest","false");
    context.setVariable("customErrorDetail","Please provide a valid cdgId");
    context.setVariable("customStatusMessage","Bad Request");
    
} else if(email === null || !regex.test(email)) {
    context.setVariable("isValidRequest","false");
    context.setVariable("customErrorDetail","Please provide a valid email");
    context.setVariable("customStatusMessage","Bad Request");
}


var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.path = path;
http.audienceId = context.getVariable("private.audienceId");
http.pem = context.getVariable("private.pem");

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var opa_request = {};
opa_request.attributes = attributes;

context.setVariable("opa_request", JSON.stringify(opa_request));


